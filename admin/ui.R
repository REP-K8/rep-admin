library(shiny)

options(shiny.reactlog=TRUE)

# Define UI
shinyUI(fluidPage(

  # Application title
  titlePanel("REnterprise - CrispML Console MoreLive"),
  p(strong("Frontend to REST API of Training/Prediction Engine")),

  # Sidebar
  sidebarLayout(
    sidebarPanel(

      # Test DB Connection
      actionButton("btnDBTables", "ListDBTables"), br(),

      # DataIngest
      actionButton("btnDataIngest", "DataIngest"),
      numericInput("numPercentData", "percentData", 1), p(""),

      "----------------------------------------------------------",
      br(), strong("Modelling"),
      p("----------------------------------------------------------"),

      # DataSelect
      sliderInput("sldrDataSelect", "Training Data: TimeFrame",
                  min=1, max=12, value=c(1,6)),

      # DataPrepare
      actionButton("btnDataPrepare", "DataPrepare"),br(),

      # DataCurate
      actionButton("btnDataCurate", "DataCurate"),br(),

      # ModelTrain
      actionButton("btnModelTrain", "ModelTrain"),
      textInput("txtModel", "Model", "rf"),
      numericInput("numWeight", "Weight", 100), br(),
      #textInput("txtParms", "Params", "weigth=100"), br(),

      # ModelAssess
      actionButton("btnModelAssess", "ModelAssess"),p(""),

      "----------------------------------------------------------",
      br(), strong("Prediction"),
      p("----------------------------------------------------------"),

      # Time Frame
      sliderInput("sliPredictFrame", "Predict Data: TimeFrame",
            min=1, max=12, value=c(7,12)),

      # PredictPrepare
      actionButton("btnPredictPrepare", "PredictPrepare"),br(),

      # PredictCurate
      actionButton("btnPredictCurate", "PredictCurate"), br(),

      # PredictScore
      actionButton("btnPredictScore", "PredictScore"),br(),

      # PredictAssess
      actionButton("btnPredictAssess", "PredictAssess"),

      width=4
    ),

    # Main panel
    mainPanel(
      h3("CrispML: Response from remote REST interfaces", align = "left"),
      h4(strong("First select Tab, then Press corresponding Button, then Wait until Output appears on Tab"), style="color:green"),

      tabsetPanel(id="crispPanel", type = "tabs",

#        tabPanel("MONITOR",
#                 h4("Daten aller Messstationen der letzten Stunde", align = "left", style="color:gray"),
#                 p("[Menüauswahl: Button 'Monitor Aktualisieren']", align = "left", style="color:green"),
#                 plotOutput("deviceMonitorPO", height = "200vh")),

        tabPanel("DBTables",
                 verbatimTextOutput("vtoDBTables")),

        tabPanel("DataIngest",
                 dataTableOutput("dtoDataIngest")),

        tabPanel("DataPrepare",
                 dataTableOutput("dtoDataPrepare")),

        tabPanel("DataCurate",
                 plotOutput("poDataCurate")),

        tabPanel("ModelTrain",
                 plotOutput("poModelTrain"))


        #tabPanel("more",
                #dataTableOutput("dtoModelTrain")
                #plotOutput("plotOutputID")
                #dataTableOutput("dataTableOutputID")
                #tableOutput("tableOutputID")
                #textOutput("textOutputID")
         # )
        ), width=8
      )
    )
  )
)
