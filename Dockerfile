# Shiny app based on rocker
FROM rocker/shiny:3.6.1

# Required by httr
RUN apt-get update && apt-get install libssl-dev libcurl4-openssl-dev -y
RUN apt-get install libpng-dev -y

# Install required libraries
RUN R -e "install.packages('shinydashboard')"
RUN R -e "install.packages('httr')"
RUN R -e "install.packages('jsonlite')"
RUN R -e "install.packages('dplyr')"
RUN R -e "install.packages('magrittr')"
RUN R -e "install.packages('influxdbr')"
RUN R -e "install.packages('png')"

# write application log to /var/log/rshiny.log
RUN touch /var/log/rshiny.log && chown shiny:shiny /var/log/rshiny.log

RUN rm -rf /srv/shiny-server/*
WORKDIR /srv/shiny-server/admin
COPY ./admin .
COPY ./share /share
RUN sudo chown -R shiny:shiny /srv/shiny-server

# Pass Kubernetes variable if available
ENV REP_CRISPML_ENV=${REP_CRISPML_ENV}

EXPOSE 3838

CMD ["/usr/bin/shiny-server.sh"]
