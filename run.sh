#! /bin/bash

# This file documents all steps necessary to run the rep-admin docker image locally

docker run --rm -p 3838:3838 --network host --name rep-admin rep-admin:latest
